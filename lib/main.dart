import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather_app/pages/fading_page/fading_page.dart';
import 'package:weather_app/shared/size_config.dart';

void main() {
if (SizeConfig.isMobilePortrait == false) {
   WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((value) => runApp(MyApp()));
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: FadingPage(),
            );
          },
        );
      },
    );
  }
}
