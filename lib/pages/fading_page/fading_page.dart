import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather_app/pages/first_page/first_page.dart';
import 'package:weather_app/shared/constants.dart';

import '../../shared/size_config.dart';

class FadingPage extends StatefulWidget {
  FadingPage({Key key}) : super(key: key);

  @override
  _FadingPageState createState() => _FadingPageState();
}

class _FadingPageState extends State<FadingPage> with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(milliseconds: 3500), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Navigator.of(context).pushReplacement(
          PageRouteBuilder(
            pageBuilder: (context, animation, anotherAnimation) {
              return FirstPage();
            },
            transitionDuration: Duration(seconds: 3),
            transitionsBuilder: (context, animation, antoherAnimation, child) {
              animation =
                  CurvedAnimation(curve: Curves.easeIn, parent: animation);
              return FadeTransition(opacity: animation, child: child);
            },
          ),
        );
      }
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        body: SizedBox(
          width: double.infinity,
          child: Column(
      children: <Widget>[
          SizedBox(
            height: getProportionateScreenWidth(60),
          ),
          Text(
            "Weather App",
            style: TextStyle(
              fontSize: getProportionateScreenWidth(36),
              color: primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: getProportionateScreenWidth(60),
          ),
      Column(
                  children: [
                    Container(
                            height:  getProportionateScreenWidth(240),
                        width:  getProportionateScreenWidth(240),
                      child: SvgPicture.asset(
                        'assets/images/weather_app.svg',
                  fit: BoxFit.cover

                      ),

                    ),
                    SizedBox(
            height: getProportionateScreenWidth(60),
          ),
                    Text(
                  'Welcome to my Weather App, Let’s check the weather together!',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: getProportionateScreenWidth(18)),
                ),
                  ],
                )
          
      ],
    ),
        )
      ),
    );
  }
}
