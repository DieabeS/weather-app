import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/controllers/weather_controller.dart';
import 'package:weather_app/pages/second_page/second_page.dart';
import 'package:weather_app/shared/constants.dart';
import 'package:weather_app/shared/size_config.dart';

class FirstPage extends StatelessWidget {
  final WeatherController controller = Get.put(WeatherController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Choose a Date',
          style: TextStyle(
              fontSize: getProportionateScreenWidth(30),
              color: Colors.black,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (controller.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return Container(
                decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              'assets/images/weather.jpg',
                            ),
                            fit: BoxFit.cover),
                      ),
                      width: double.infinity,
                      height: double.infinity,
              child: ListView.builder(
                itemCount: controller.dateList.length,
                itemBuilder: (context, i) {
                  return Card(color: Colors.transparent,
                    elevation: 1,
                    child: ListTile(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SecondPage(
                                hours: controller
                                    .getHours(controller.data[i]['dt_txt']),
                                date: '${controller.dateList[i]}',
                              ),
                            ),
                          );
                        },
                        title: Center(
                          child: controller.dateList != null
                              ? Hero(tag: 'date-${controller.dateList[i]}',
                                        child: Material(type:  MaterialType.transparency,
                                                                                  child: Text('${controller.dateList[i]}',
                                    style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: getProportionateScreenWidth(25),
                    color: Colors.white)),
                                        ),
                              )
                              : Text(''),
                        ),
                        subtitle: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            controller.data != null
                                ? Text(
                                    '${(controller.data[i]['main']['temp'] - 273.15).round().toString()}\u2103 ',
                                    style: textStyle())
                                : Text(''),
                            controller.data != null
                                ? Text(
                                    '${(controller.data[i]['weather'][0]['main'])}',
                                    style: textStyle())
                                : Text('')
                          ],
                        ),
                      ),
                  );
                },
              ),
            );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.refreshWeather();
        },
        child: Icon(
          Icons.refresh,
          size: getProportionateScreenWidth(30),
        ),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white
      ),
    );
  }
}
