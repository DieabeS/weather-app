import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:weather_app/models/weather.dart';
import 'dart:convert';

class Api {
  Future<WeatherList> getWeatherList() async {
    String url =
        'http://api.openweathermap.org/data/2.5/forecast?id=292223&appid=8925480114759bf525fdade7a857b52c';
    String fileName = 'weatherData.json';
    var dir = await getTemporaryDirectory();

    File file = new File(dir.path + '/' + fileName);
    if (file.existsSync()) {

      var jsonData = file.readAsStringSync();
      return WeatherList.fromJson(json.decode(jsonData));
    } else {
      final response = await http.get(url);
      print(response.statusCode);

      if (response.statusCode == 200) {

        file.writeAsStringSync(response.body,
            flush: true, mode: FileMode.write);

        return WeatherList.fromJson(json.decode(response.body));
      } else
        throw Exception('Cannot load Weather data');
    }
  }

  Future<WeatherList> refreshWeatherList() async {
    String url =
        'http://api.openweathermap.org/data/2.5/forecast?id=292223&appid=8925480114759bf525fdade7a857b52c';

    final response = await http.get(url);
    print(response.statusCode);
 String fileName = 'weatherData.json';
    var dir = await getTemporaryDirectory();

    File file = new File(dir.path + '/' + fileName);

    if (response.statusCode == 200) {
        print('Loading From Api');

file.writeAsStringSync(response.body,
            flush: true, mode: FileMode.write);
      return WeatherList.fromJson(json.decode(response.body));
    } else
      throw Exception('Cannot load Weather data');
  }
}
