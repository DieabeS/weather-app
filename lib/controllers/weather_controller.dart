import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/api/api.dart';
import 'package:weather_app/models/weather.dart';

class WeatherController extends GetxController {
  var isLoading = true.obs;
  List weatherList = List<WeatherList>().obs;
  List dateList = List<String>().obs;
  List data = List().obs;

  @override
  void onInit() async {
    getWeatherList();
    super.onInit();
  }

  void getWeatherList() async {
    try {
      isLoading(true);
      Api api = new Api();
      await api.getWeatherList().then((value) {
        weatherList = value.list;
      });

      for (int i = 0; i < weatherList.length; i++) {
        if (!dateList.contains(DateFormat()
            .add_yMMMMEEEEd()
            .format(DateTime.parse(weatherList[i]['dt_txt'])))) {
          dateList.add(DateFormat()
              .add_yMMMMEEEEd()
              .format(DateTime.parse(weatherList[i]['dt_txt'])));
          data.add(weatherList[i]);
        }
      }
 
    } finally {
      isLoading(false);
    }
  }

  void refreshWeather() async {
    try {
      isLoading(true);
      Api api = new Api();
      await api.refreshWeatherList().then((value) {
        weatherList = value.list;
      });
      dateList.clear();
      data.clear();

      for (int i = 0; i < weatherList.length; i++) {
        if (!dateList.contains(DateFormat()
            .add_yMMMMEEEEd()
            .format(DateTime.parse(weatherList[i]['dt_txt'])))) {
          dateList.add(DateFormat()
              .add_yMMMMEEEEd()
              .format(DateTime.parse(weatherList[i]['dt_txt'])));
          data.add(weatherList[i]);
        }
      }
    } finally {
      isLoading(false);
    }
  }

  List getHours(String date) {
    DateTime selectedDay = DateTime.parse(date);
    List hours = List();
    for (int i = 0; i < weatherList.length; i++) {
      DateTime tempDate = DateTime.parse(weatherList[i]['dt_txt']);
    
      if (tempDate.year == selectedDay.year &&
          tempDate.month == selectedDay.month &&
          tempDate.day == selectedDay.day) {

            hours.add(weatherList[i]);
          }
    }
    print(weatherList);
    return hours;
  }

}
