import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/shared/constants.dart';
import 'package:weather_app/shared/size_config.dart';

class SecondPage extends StatefulWidget {
  final String date;
  final List hours;

  SecondPage({
    Key key,
    @required this.date,
    @required this.hours,
  }) : super(key: key);

  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    String bgImg;

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          flexibleSpace: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(
                    widget.hours.length,
                    (index) => buildDot(index: index),
                  ),
                ),
              ),
            ],
          ),
          elevation: 0,
        ),
        body: Column(
          children: [
            Expanded(
              child: PageView.builder(
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemCount: widget.hours.length,
                  itemBuilder: (context, i) {
                    if (widget.hours[i]['weather'][0]['main'] == 'Clear') {
                      bgImg = 'assets/images/clear.jpg';
                    } else if (widget.hours[i]['weather'][0]['main'] ==
                        'Clouds') {
                      bgImg = 'assets/images/cloudy.jpg';
                    } else if (widget.hours[i]['weather'][0]['main'] ==
                        'Rain') {
                      bgImg = 'assets/images/rain.jpg';
                    } else if (widget.hours[i]['weather'][0]['main'] ==
                        'Snow') {
                      bgImg = 'assets/images/snow.jpg';
                    }
                    return Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              bgImg,
                            ),
                            fit: BoxFit.cover),
                      ),
                      width: double.infinity,
                      height: double.infinity,
                      child: Padding(
                        padding:
                            EdgeInsets.all(getProportionateScreenWidth(20)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: getProportionateScreenWidth(90),
                            ),
                            Text(
                              'Dubai',
                              style: TextStyle(
                                  fontSize: getProportionateScreenWidth(50),
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            Row(
                              children: [
                                Text(
                                  '${DateFormat("h:mma").format(DateTime.parse(widget.hours[i]['dt_txt']))}',
                                  style: TextStyle(
                                      fontSize: getProportionateScreenWidth(18),
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                SizedBox(
                                  width: getProportionateScreenWidth(10),
                                ),
                                Hero(
                                  tag: 'date-${widget.date}',
                                  child: Material( type:MaterialType.transparency,
                                                                      child: Text(
                                      widget.date,
                                      style: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(18),
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            Text(
                              '${(widget.hours[i]['main']['temp'] - 273.15).round()}\u2103',
                              style: TextStyle(
                                  fontSize: getProportionateScreenWidth(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              widget.hours[i]['weather'][0]['main'],
                              style: TextStyle(
                                  fontSize: getProportionateScreenWidth(19),
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(
                              color: Colors.white,
                              thickness: getProportionateScreenWidth(3),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.thermometerHalf,
                                  size: getProportionateScreenWidth(25),
                                  color: Colors.white,
                                ),
                                Text(
                                  'Temperature',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['main']['temp'] - 273.15).round()}\u2103',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.thermometerHalf,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Feels Like',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['main']['feels_like'] - 273.15).round()}\u2103',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.thermometerEmpty,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Minimum Temperature',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['main']['temp_min'] - 273.15).round()}\u2103',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.thermometerFull,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Maximum Temperature',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['main']['temp_max'] - 273.15).round()}\u2103',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.cloud,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Weather',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['weather'][0]['main'])}',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.stream,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Humidity',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['main']['humidity'])}%',
                                  style: textStyle(),
                                )
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenWidth(20),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.wind,
                                  color: Colors.white,
                                  size: getProportionateScreenWidth(25),
                                ),
                                Text(
                                  'Wind Speed',
                                  style: textStyle(),
                                ),
                                Text(
                                  '${(widget.hours[i]['wind']['speed'])}',
                                  style: textStyle(),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ));
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      margin: EdgeInsets.only(right: getProportionateScreenWidth(8)),
      height: getProportionateScreenWidth(6),
      width: currentPage == index
          ? getProportionateScreenWidth(20)
          : getProportionateScreenWidth(6),
      decoration: BoxDecoration(
        color: currentPage == index ? Colors.white : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(getProportionateScreenWidth(3)),
      ),
    );
  }
}
